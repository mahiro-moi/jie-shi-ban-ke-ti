package moi_mahiro.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import moi_mahiro.beans.User;


@WebFilter(filterName ="loginFilter")
public class LoginFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain){
	    try{

	    	Object session = ((HttpServletRequest) request).getSession().getAttribute("loginUser");

	    	User user = (User) session;

	    	if (session != null){

		    	if(user.getId() != 0) {
		    		chain.doFilter(request, response);
		    	}else {

		    		HttpSession sessions = ((HttpServletRequest) request).getSession();
		    		List<String> messages = new ArrayList<String>();
		           	messages.add("ログインしてください");
		            sessions.setAttribute("errorMessages", messages);

		            HttpServletResponse res = ((HttpServletResponse) response);
		            res.sendRedirect("./login");
		    	}
	    	}else {

	    		HttpSession sessions = ((HttpServletRequest) request).getSession();
	    		List<String> messages = new ArrayList<String>();
	           	messages.add("ログインしてください");
	            sessions.setAttribute("errorMessages", messages);

	            HttpServletResponse res = ((HttpServletResponse) response);
	            res.sendRedirect("./login");
	    	}
	    }catch (ServletException se){
	    }catch (IOException e){
	    }
	  }
    public void init(FilterConfig config) throws ServletException{}
    public void destroy(){}
}