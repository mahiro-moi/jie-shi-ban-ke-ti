package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import moi_mahiro.beans.Branches;
import moi_mahiro.beans.Departments;
import moi_mahiro.beans.User;
import moi_mahiro.dao.UserDao;
import moi_mahiro.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User idCheck(String login_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.idCheck(connection, login_id);

            commit(connection);
            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getUser(String login_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, login_id);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public List<Branches> getBranch(Branches branch){

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<Branches> ret = userDao.getBranch(connection, branch);
            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public List<Departments> getDepartment(Departments department){

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            List<Departments> ret = userDao.getDepartment(connection, department);
            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}