package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import moi_mahiro.beans.Management;
import moi_mahiro.dao.ManagementDao;
import moi_mahiro.dao.UserManagementDao;

public class ManagementService {

    public void edit(Management management) {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao managementDao = new ManagementDao();
            managementDao.insert(connection, management);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<Management> getManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserManagementDao managementDao = new UserManagementDao();
            List<Management> ret = managementDao.getUserManagement(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}