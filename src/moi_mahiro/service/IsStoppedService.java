package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;

import moi_mahiro.beans.Management;
import moi_mahiro.dao.ManagementDao;

public class IsStoppedService {

    public void isStopped(Management management) {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao managementDao = new ManagementDao();
            managementDao.isStopped(connection, management);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}