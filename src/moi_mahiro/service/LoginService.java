package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;

import moi_mahiro.beans.User;
import moi_mahiro.dao.UserDao;
import moi_mahiro.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User loginEdit(String login_id) {

            Connection connection = null;
            try {
                connection = getConnection();

                UserDao userDao = new UserDao();
                User user = userDao.getUser(connection, login_id);

                commit(connection);

                return user;
            } catch (RuntimeException e) {
                rollback(connection);
                throw e;
            } catch (Error e) {
                rollback(connection);
                throw e;
            } finally {
                close(connection);
            }
    }
}