package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import moi_mahiro.beans.Message;
import moi_mahiro.beans.UserMessage;
import moi_mahiro.dao.MessageDao;
import moi_mahiro.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public List<UserMessage> getMessage(HttpServletRequest request)  {

        Connection connection = null;
        try {
            connection = getConnection();

            //最終的にcontrollerに記述
            UserMessage message = new UserMessage();
            String start = "2019-01-01 00:00:00";
            String end = "2099-12-31 23:59:59" ;

            if(request.getParameter("start_at") == null || request.getParameter("start_at") == "" ){
            	message.setStartAt(start);
            }else {
            	message.setStartAt(request.getParameter("start_at")+" 00:00:00");
            }
            if(request.getParameter("end_at") == null || request.getParameter("end_at") == "") {
            	message.setEndAt(end);
            }else {
            	message.setEndAt(request.getParameter("end_at")+" 23:59:59");
            }
            if(request.getParameter("category") != null || request.getParameter("category") != ""){
            	message.setCategory(request.getParameter("category"));
            }

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection,message,request);
            ret.add(message);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
		} finally {
            close(connection);
        }
    }
}