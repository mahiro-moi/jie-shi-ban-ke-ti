package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import moi_mahiro.beans.Comment;
import moi_mahiro.beans.UserComment;
import moi_mahiro.dao.CommentDao;
import moi_mahiro.dao.UserCommentDao;

public class CommentService {

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao commentDao = new UserCommentDao();
            List<UserComment> ret = commentDao.getUserComment(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }



}