package moi_mahiro.service;

import static moi_mahiro.utils.CloseableUtil.*;
import static moi_mahiro.utils.DBUtil.*;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;

import moi_mahiro.beans.User;
import moi_mahiro.dao.EditDao;
import moi_mahiro.utils.CipherUtil;

public class EditService {

    public void register(User user,HttpServletRequest request) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            EditDao editDao = new EditDao();
            editDao.updatde(connection, user,request);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getUser(User user ) {

        Connection connection = null;
        try {
            connection = getConnection();

            EditDao editDao = new EditDao();
            User ret = editDao.getUser(connection, user);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}