package moi_mahiro.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String login_id;
    private String name;
    private String password;
    private int branch_id;
    private String branch_name;
    private int department_id;
    private String department_name;
    private String is_stopped;
    private Date created_date;
    private Date updated_date;




    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    public void setLoginId(String login_id) {
    	this.login_id = login_id;
    }
    public String getLoginId() {
    	return login_id;
    }
    public void setPassword(String password) {
    	this.password = password;
    }
    public String getPassword() {
    	return password;
    }
    public void setBranchId(int branch_id) {
    	this.branch_id = branch_id;
    }
    public int getBranchId() {
    	return branch_id;
    }
    public void setDepartmentId(int department_id) {
    	this.department_id = department_id;
    }
    public int getDepartmentId() {
    	return department_id;
    }
    public void setIsStopped(String is_stopped) {
    	this.is_stopped = is_stopped;
    }
    public String getIsStopped() {
    	return is_stopped;
    }
    public void setCreatedDate(Date createdDate) {
    	this.created_date = createdDate;
    }
    public Date gettCreatedDate() {
    	return created_date;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updated_date = updatedDate;
    }
    public Date updatedDate() {
    	return updated_date ;
    }
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public void setBranchName(String branchName) {
    	this.branch_name = branchName;
    }
    public String getBranchName() {
    	return branch_name;
    }
    public void setDepartmentName(String departmentName) {
    	this.department_name = departmentName;
    }
    public String getDepartmentName() {
    	return department_name;
	}
	public void setId(Object id) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
