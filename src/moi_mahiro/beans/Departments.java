package moi_mahiro.beans;

import java.io.Serializable;

public class Departments implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String department_name;

    public void setId(int id) {
    	this.id = id;
    }
    public int getId() {
    	return id;
    }
    public void setDepartmentName(String departmentName) {
    	this.department_name = departmentName;
    }
    public String getDepartmentName() {
    	return department_name;
    }
}