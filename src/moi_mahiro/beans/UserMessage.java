package moi_mahiro.beans;

import java.io.Serializable;


public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String name;
    private String subject;
    private String text;
    private String category;
    private String created_date;
    private String start_at;
    private String end_at;


    public void setId(int id) {
    	this.id = id;
    }
    public int getId() {
    	return id;
    }
    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    public void setUserId(int user_id) {
    	this.user_id = user_id;
    }
    public int getUserId() {
    	return user_id;
    }
    public void setSubject(String subject) {
    	this.subject = subject;
    }
    public String getSubject() {
    	return subject;
    }
    public void setText(String text) {
    	this.text = text;
    }
    public String getText() {
    	return text;
    }
    public void setCategory(String category) {
    	this.category = category;
    }
    public String getCategory() {
    	return category;
    }
    public void setCreatedDate(String created_date) {
    	this.created_date = created_date;
    }
    public String getCreatedDate() {
    	return created_date;
	}
    public void setStartAt(String start_at) {
    	this.start_at = start_at;
    }
    public String getStartAt() {
    	return start_at;
	}
    public void setEndAt(String end_at) {
    	this.end_at = end_at;
    }
    public String getEndAt() {
    	return end_at;
	}
}
