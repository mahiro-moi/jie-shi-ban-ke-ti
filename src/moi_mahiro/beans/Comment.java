package moi_mahiro.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private int message_id;
    private String text;
    private Date created_date;
    private String name;

    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setId(int id) {
    	this.id = id;
    }
    public int getId() {
    	return id;
    }
    public void setMessageId(int message_id) {
    	this.message_id = message_id;
    }
    public int getMessageId() {
    	return message_id;
    }
    public void setUserId(int user_id) {
    	this.user_id = user_id;
    }
    public int getUserId() {
    	return user_id;
    }

    public void setText(String text) {
    	this.text = text;
    }
    public String getText() {
    	return text;
    }
    public void setCreatedDate(Date created_date) {
    	this.created_date = created_date;
    }
    public Date getCreatedDate() {
    	return created_date;
    }
}