package moi_mahiro.beans;

import java.io.Serializable;
import java.util.Date;

public class Management implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;
    private String name;
    private String password;
    private String branch_id;
    private String login_id;
    private String department_id;
    private String is_stopped;
    private String branch_name;
    private String department_name;
    private Date created_date;
    private Date updated_date;



    public void setName(String name) {
    	this.name = name;
    }
    public String getName() {
    	return name;
    }
    public void setLoginId(String login_id) {
    	this.login_id = login_id;
    }
    public String getLoginId() {
    	return login_id;
    }
    public void setDepartmentName(String department_name) {
    	this.department_name = department_name;
    }
    public String getDepartmentName() {
    	return department_name;
    }
    public void setBranchName(String branch_name) {
    	this.branch_name = branch_name;
    }
    public String getBranchName() {
    	return branch_name;
    }
    public void setPassword(String password) {
    	this.password = password;
    }
    public String getPassword() {
    	return password;
    }
    public void setBranchId(String branch_id) {
    	this.branch_id = branch_id;
    }
    public String getBranchId() {
    	return branch_id;
    }
    public void setDepartmentId(String department_id) {
    	this.department_id = department_id;
    }
    public String getDepartmentId() {
    	return department_id;
    }
    public void setIsStopped(String is_stopped) {
    	this.is_stopped = is_stopped;
    }
    public String getIsStopped() {
    	return is_stopped;
    }
    public void setCreatedDate(Date createdDate) {
    	this.created_date = createdDate;
    }
    public Date getCreatedDate() {
    	return created_date;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updated_date = updatedDate;
    }
    public Date updatedDate() {
    	return updated_date ;
    }
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
}
