package moi_mahiro.beans;

import java.io.Serializable;

public class Branches implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String branch_name;

    public void setId(int id) {
    	this.id = id;
    }
    public int getId() {
    	return id;
    }
    public void setBranchName(String branchName) {
    	this.branch_name = branchName;
    }
    public String getBranchName() {
    	return branch_name;
    }
}