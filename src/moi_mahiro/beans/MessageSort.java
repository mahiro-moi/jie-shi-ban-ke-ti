package moi_mahiro.beans;

import java.io.Serializable;


public class MessageSort implements Serializable {
    private static final long serialVersionUID = 1L;

    private String category;
    private String start_at;
    private String end_at;


    public void setCategory(String category) {
    	this.category = category;
    }
    public String getCategory() {
    	return category;
    }
    public void setStartAt(String start_at) {
    	this.start_at = start_at;
    }
    public String getStartAt() {
    	return start_at;
	}
    public void setEndAt(String end_at) {
    	this.end_at = end_at;
    }
    public String getEndAt() {
    	return end_at;
	}
}
