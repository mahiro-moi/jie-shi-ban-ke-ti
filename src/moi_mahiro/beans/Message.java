package moi_mahiro.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int user_id;
    private String subject;
    private String text;
    private String category;
    private Date created_date;

    public void setId(int id) {
    	this.id = id;
    }
    public int getId() {
    	return id;
    }
    public void setUserId(int user_id) {
    	this.user_id = user_id;
    }
    public int getUserId() {
    	return user_id;
    }
    public void setSubject(String subject) {
    	this.subject = subject;
    }
    public String getSubject() {
    	return subject;
    }
    public void setText(String text) {
    	this.text = text;
    }
    public String getText() {
    	return text;
    }
    public void setCategory(String category) {
    	this.category = category;
    }
    public String getCategory() {
    	return category;
    }
    public void setCreatedDate(Date created_date) {
    	this.created_date = created_date;
    }
    public Date getCreatedDate() {
    	return created_date;
    }
}