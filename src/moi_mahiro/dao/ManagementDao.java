package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import moi_mahiro.beans.Management;
import moi_mahiro.exception.SQLRuntimeException;

public class ManagementDao {

    public void insert(Connection connection, Management management) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("branch_id");
            sql.append(", department_id");
            sql.append(", name");
            sql.append(", is_stopped");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", ?"); // name
            sql.append(", ?"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // update_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, management.getBranchId());
            ps.setString(2, management.getDepartmentId());
            ps.setString(3, management.getName());
            ps.setString(4, management.getIsStopped());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void isStopped( Connection connection, Management management) {
    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("UPDATE users SET  ");
             sql.append("is_stopped = ");
             sql.append("?");
             sql.append(" WHERE ");
             sql.append("id = ");
             sql.append("?");
             sql.append(";");

             ps = connection.prepareStatement(sql.toString());

             ps.setString(1, management.getIsStopped());
             ps.setInt(2, management.getId());
             ps.executeUpdate();



         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
}