package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import moi_mahiro.beans.Branches;
import moi_mahiro.beans.Departments;
import moi_mahiro.beans.User;
import moi_mahiro.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // department_id
            sql.append(", 0"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String login_Id, String password
           ) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
        	 sql.append("SELECT * FROM ");
        	 sql.append(" users");
        	 sql.append(" WHERE login_id =");
        	 sql.append(" ?");
        	 sql.append(" AND password =");
        	 sql.append(" ?");
        	 sql.append(";");


            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, login_Id);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String login_id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setString(1, login_id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int branchId = rs.getInt("branch_id");
                int departmentId = rs.getInt("department_id");
                String isStopped = rs.getString("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setName(name);
                user.setBranchId(branchId);
                user.setDepartmentId(departmentId);
                user.setIsStopped(isStopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<Branches> getBranch(Connection connection, Branches branch) {

         PreparedStatement ps = null;
         try {
             String sql = "SELECT * FROM branches;";

             ps = connection.prepareStatement(sql);
             ResultSet rs = ps.executeQuery();
             List<Branches> branchList = toBranchList(rs);

             return branchList;

         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
     }
    private List<Branches> toBranchList(ResultSet rs) throws SQLException {

    	List<Branches> ret = new ArrayList<Branches>();
    	try {
    		while (rs.next()) {
    			int id = rs.getInt("id");
    			String branchName = rs.getString("branch_name");

    			Branches branch = new Branches();
    			branch.setId(id);
    			branch.setBranchName(branchName);
    			ret.add(branch);
        }
        return ret;
    	} finally {
    		close(rs);
    	}
    }
    public List<Departments> getDepartment(Connection connection, Departments department) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM Departments;";

            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Departments> departmentList = toDepartmentList(rs);

            return departmentList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    private List<Departments> toDepartmentList(ResultSet rs) throws SQLException {

    	List<Departments> ret = new ArrayList<Departments>();
    	try {
    		while (rs.next()) {
    			int id = rs.getInt("id");
    			String departmentName = rs.getString("department_name");

    			Departments department = new Departments();
    			department.setId(id);
    			department.setDepartmentName(departmentName);
    			ret.add(department);
        }
        return ret;
    	} finally {
    		close(rs);
    	}
    }
    public User idCheck (Connection connection,String login_id){

    	PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
    		sql.append("FROM users ");
            sql.append("WHERE ");
            sql.append("users.login_id = ");
            sql.append("?");
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, login_id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}