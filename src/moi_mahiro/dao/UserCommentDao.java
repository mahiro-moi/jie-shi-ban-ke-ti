package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import moi_mahiro.beans.UserComment;
import moi_mahiro.exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComment(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
		    sql.append("SELECT ");
		    sql.append("users.name, ");
		    sql.append("comments.id,");
		    sql.append("comments.user_id, ");
		    sql.append("comments.text, ");
		    sql.append("comments.message_id, ");
		    sql.append("comments.created_date ");
		    sql.append("FROM comments ");
		    sql.append("INNER JOIN messages ");
		    sql.append("ON messages.id = comments.message_id  ");
		    sql.append("INNER JOIN users ");
		    sql.append("ON comments.user_id = users.id  ");

		    ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
	            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<UserComment> toUserCommentList(ResultSet rs)
	            throws SQLException {
		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {

				String name = rs.getString("name");
	            String text = rs.getString("text");
	            int userId = rs.getInt("user_id");
	            int commentId = rs.getInt("id");
	            int messageId = rs.getInt("message_id");
	            Timestamp tsCreatedDate = rs.getTimestamp("created_date");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
                String createdDate = sdf.format(tsCreatedDate);

	            UserComment comment = new UserComment();
	            comment.setName(name);
	            comment.setText(text);
	            comment.setMessageId(messageId);
	            comment.setId(commentId);
	            comment.setCreatedDate(createdDate);
	            comment.setUserId(userId);

	            ret.add(comment);
			}
			return ret;
	        }finally {
	        	close(rs);
	        }
	}
}