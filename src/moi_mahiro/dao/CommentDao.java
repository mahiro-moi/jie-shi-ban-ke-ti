package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import moi_mahiro.beans.Comment;
import moi_mahiro.exception.SQLRuntimeException;

public class CommentDao {

		public void insert(Connection connection , Comment comment) {
			 PreparedStatement ps = null;
		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("INSERT INTO comments ( ");
		            sql.append("user_id");
		            sql.append(", message_id");
		            sql.append(", text");
		            sql.append(", created_date");
		            sql.append(") VALUES (");
		            sql.append(" ?"); // user_id
		            sql.append(", ?"); // message_id
		            sql.append(", ?"); // text
		            sql.append(", CURRENT_TIMESTAMP"); // created_date
		            sql.append(")");

		            ps = connection.prepareStatement(sql.toString());

		            ps.setInt(1, comment.getUserId());
		            ps.setInt(2, comment.getMessageId());
		            ps.setString(3, comment.getText());
		            ps.executeUpdate();

		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
		}
		public void delete(Connection connection , Comment comment) {
			 PreparedStatement ps = null;
		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("Delete from comments Where id =");
		            sql.append("?");
		            sql.append(";");

		            ps = connection.prepareStatement(sql.toString());
		            ps.setInt(1, comment.getId());
		            ps.executeUpdate();

		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
		}
}