package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import moi_mahiro.beans.Management;
import moi_mahiro.exception.SQLRuntimeException;

public class UserManagementDao {

	public List<Management> getUserManagement(Connection connection) {
		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
    		sql.append("users.name, ");
    		sql.append("users.id, ");
    		sql.append("users.password, ");
    		sql.append("users.department_id,");
    		sql.append("users.branch_id, ");
    		sql.append("branches.branch_name, ");
    		sql.append("departments.department_name, ");
    		sql.append("users.is_stopped, ");
    		sql.append("users.updated_date, ");
    		sql.append("users.login_id ");
    		sql.append("FROM users ");
    		sql.append("INNER JOIN branches ");
    		sql.append("ON users.branch_id = branches.id  ");
    		sql.append("INNER JOIN departments ");
    		sql.append("ON users.department_id = departments.id  ");
    		sql.append("ORDER BY created_date ASC limit " + 1000);

    		ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Management> ret = toUserManagementList(rs);
	            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private List<Management> toUserManagementList(ResultSet rs)
	            throws SQLException {
		List<Management> ret = new ArrayList<Management>();
		try {
			while (rs.next()) {

				String name = rs.getString("name");
				int id = rs.getInt("id");
				String departmentId = rs.getString("department_id");
				String departmentName = rs.getString("department_name");
				String branchName = rs.getString("branch_name");
				String password = rs.getString("password");
				String branchId = rs.getString("branch_id");
				String isStopped = rs.getString("is_stopped");
				String loginId = rs.getString("login_id");
	            Timestamp updateDate = rs.getTimestamp("updated_date");

	            Management management = new Management();
	            management.setName(name);
	            management.setId(id);
	            management.setPassword(password);
	            management.setDepartmentId(departmentId);
	            management.setDepartmentName(departmentName);
	            management.setBranchName(branchName);
	            management.setBranchId(branchId);
	            management.setIsStopped(isStopped);
	            management.setUpdatedDate(updateDate);
	            management.setLoginId(loginId);


	            ret.add(management);
			}
			return ret;
	        }finally {
	        	close(rs);
	        }
	}
}