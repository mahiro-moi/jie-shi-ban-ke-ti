package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import moi_mahiro.beans.UserMessage;
import moi_mahiro.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection,UserMessage message,HttpServletRequest request) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.category as category, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date ");
            sql.append("BETWEEN ");
            sql.append("? ");
            sql.append("AND ");
            sql.append("?");
            if(request.getParameter("category") != null) {
            	sql.append("AND messages.category LIKE ");
                sql.append("?");
            }
            	sql.append("ORDER BY created_date DESC limit " + 1000);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, message.getStartAt());
            ps.setString(2, message.getEndAt());
            if(request.getParameter("category") != null) {
                ps.setString(3, "%" + message.getCategory() + "%");
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs,request);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs,HttpServletRequest request)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String name = rs.getString("name");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp tsCreatedDate = rs.getTimestamp("created_date");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:SS");
                String createdDate = sdf.format(tsCreatedDate);

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setName(name);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}