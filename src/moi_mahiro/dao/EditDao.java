package moi_mahiro.dao;

import static moi_mahiro.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import moi_mahiro.beans.User;
import moi_mahiro.exception.SQLRuntimeException;

public class EditDao {
	 public void updatde( Connection connection, User user,HttpServletRequest request) {
    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("UPDATE users SET  ");
             sql.append("name = ");
             sql.append("?,");
             if(request.getParameter("password") != "") {
             sql.append("password = ");
             sql.append("?,");
             }
             sql.append("login_id = ");
             sql.append("?,");
             sql.append("branch_id = ");
             sql.append("?,");
             sql.append("department_id = ");
             sql.append("?");
             sql.append(" WHERE ");
             sql.append("id = ");
             sql.append("?");
             sql.append(";");

             ps = connection.prepareStatement(sql.toString());

             ps.setString(1, user.getName());
             if(request.getParameter("password") != "") {
            	 ps.setString(2, user.getPassword());
            	 ps.setString(3, user.getLoginId());
            	 ps.setInt(4, user.getBranchId());
            	 ps.setInt(5, user.getDepartmentId());
            	 ps.setInt(6, user.getId());
             }else if(request.getParameter("password") == "") {
            	 ps.setString(2, user.getLoginId());
                 ps.setInt(3, user.getBranchId());
                 ps.setInt(4, user.getDepartmentId());
                 ps.setInt(5, user.getId());
             }
             ps.executeUpdate();

         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
	 public User getUser(Connection connection, User user) {
    	 PreparedStatement ps = null;
         try {
             StringBuilder sql = new StringBuilder();
             sql.append("SELECT ");
             sql.append(" * ");
     		 sql.append("FROM users ");
             sql.append("WHERE ");
             sql.append("users.id = ");
             sql.append("?");
             sql.append(";");

             ps = connection.prepareStatement(sql.toString());

             ps.setInt(1, user.getId());
             ResultSet rs = ps.executeQuery();
             User users = toUserList(rs);
             return users;

         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
	 private User toUserList(ResultSet rs)
	            throws SQLException {
		User user = new User();
		try {
			while (rs.next()) {

				String name = rs.getString("name");
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
			    int departmentId = rs.getInt("department_id");
				//String password = rs.getString("password");
				int branchId = rs.getInt("branch_id");
	            Timestamp updateDate = rs.getTimestamp("updated_date");

	            user.setName(name);
	            user.setId(id);
	            //user.setPassword(password);
	            user.setPassword("");
	            user.setDepartmentId(departmentId);
	            user.setBranchId(branchId);
	            user.setUpdatedDate(updateDate);
	            user.setLoginId(loginId);
			}
			return user;
	        }finally {
	        	close(rs);
	        }
	}
}
