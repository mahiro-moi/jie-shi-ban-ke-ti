package moi_mahiro.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import moi_mahiro.beans.Comment;
import moi_mahiro.service.DeleteService;

	@WebServlet(urlPatterns = { "/deleteComment" })
	public class CommentDeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	    	Comment comment = new Comment();
	    	comment.setId(Integer.parseInt(request.getParameter("commentId")));

	    	new DeleteService().commentDelete(comment);

	            response.sendRedirect("./");
	    }
	}