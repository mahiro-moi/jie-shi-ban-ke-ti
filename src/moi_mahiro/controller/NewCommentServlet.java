package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import moi_mahiro.beans.Comment;
import moi_mahiro.service.CommentService;

	@WebServlet(urlPatterns = { "/newComment" })
	public class NewCommentServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	    	Comment comment = createCommentData(request);

			List<String> errorMessages = new ArrayList<String>();

			boolean isSuccess = false;
			String responseJson = "";
			String errors = "";

			if(isValid(comment, errorMessages)) {
	            new CommentService().register(comment);
				isSuccess = true;
				responseJson = String.format("{\"is_success\" : \"%s\"}", isSuccess);

	        } else {
				errors = new ObjectMapper().writeValueAsString(errorMessages);
				responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
	        }

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(responseJson);
	    }
			private Comment createCommentData(HttpServletRequest request) {
				String data = request.getParameter("comment");

				Comment comment = new Comment();
				try {
					comment = new ObjectMapper().readValue(data, Comment.class);
				} catch(Exception e) {
					e.printStackTrace();
				}

				return comment;
			}
			private boolean isValid(Comment comment, List<String> errorMessages) {


	        if (StringUtils.isBlank(comment.getText())) {
	        	errorMessages.add("コメントを入力してください");
	        }else if (500 < comment.getText().length()) {
	        	errorMessages.add("コメントは500文字以下で入力してください");
	        }
	        if (errorMessages.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	    }

	}