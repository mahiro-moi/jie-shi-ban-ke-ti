package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import moi_mahiro.beans.UserComment;
import moi_mahiro.beans.UserMessage;
import moi_mahiro.service.CommentService;
import moi_mahiro.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException  {


    	List<String> errorMessages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, errorMessages)) {

        	List<UserMessage> messages = new MessageService().getMessage(request);

        	List<UserComment> comments = new CommentService().getComment();

        	String startAt = request.getParameter("start_at");
        	String endAt = request.getParameter("end_at");
        	String category = request.getParameter("category");

        	request.setAttribute("start_at", startAt);
        	request.setAttribute("end_at", endAt);
        	request.setAttribute("category", category);


        	request.setAttribute("messages", messages);
        	request.setAttribute("comments", comments);

        	request.getRequestDispatcher("/top.jsp").forward(request, response);

        }else {
            session.setAttribute("errorMessages", errorMessages);

            List<UserMessage> messages = new MessageService().getMessage(request);
            List<UserComment> comments = new CommentService().getComment();

            String startAt = request.getParameter("start_at");
            String endAt = request.getParameter("end_at");
            String category = request.getParameter("category");

            request.setAttribute("start_at", startAt);
            request.setAttribute("end_at", endAt);
            request.setAttribute("category", category);

            request.setAttribute("messages", messages);
            request.setAttribute("comments", comments);


            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }
    }
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	String startAt = request.getParameter("start_at");
        String endAt = request.getParameter("end_at");
        String category = request.getParameter("category");


        if(category == null) {
        	return true;
        }else if(category.startsWith(" ")) {
        	messages.add("カテゴリーに不正な文字が入力されています");
        }else if(category.startsWith("　")) {
        	messages.add("カテゴリーに不正な文字が入力されています");
        }
        if(StringUtils.isBlank(request.getParameter("end_at"))) {
        	endAt = "2099-12-31 23:59:59" ;
        }
        if(startAt.compareTo(endAt) > 0) {
        	messages.add("期間指定が不正です");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}