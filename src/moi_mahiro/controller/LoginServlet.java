package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import moi_mahiro.beans.User;
import moi_mahiro.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

    	if (isValid(request, errorMessages)) {

    		String login_id = request.getParameter("login_id");
    		String password = request.getParameter("password");

    		LoginService loginService = new LoginService();
    		User user = loginService.login(login_id, password);

    		session.setAttribute("loginUser", user);
    		response.sendRedirect("./");
        }else {
            session.setAttribute("errorMessages", errorMessages);

            String login_id = request.getParameter("login_id");

    		User user = new User();
    		user.setLoginId(login_id);

    		session.setAttribute("loginUser", user);

            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }
    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

    	 String login_id = request.getParameter("login_id");
         String password = request.getParameter("password");

         LoginService loginService = new LoginService();
         User user = loginService.login(login_id, password);

        if(login_id == "" || password == "") {
        	errorMessages.add("ログインIDまたはパスワードが入力されていません");
        }else if (user == null) {
        	errorMessages.add("ログインIDまたはパスワードが誤っています");
        }else if(user.getIsStopped().equals("1")) {
        	errorMessages.add("ログインIDまたはパスワードが誤っています");
        }
    	if (errorMessages.size() == 0) {
            return true;
    	}
    	else {
            return false;
    	}
    }

}