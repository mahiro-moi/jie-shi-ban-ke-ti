package moi_mahiro.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import moi_mahiro.beans.Message;
import moi_mahiro.service.DeleteService;

	@WebServlet(urlPatterns = { "/deleteMessage" })
	public class MessageDeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	    	Message message = new Message();
	    	message.setId(Integer.parseInt(request.getParameter("messageId")));

	    	new DeleteService().messageDelete(message);

	            response.sendRedirect("./");
	    }
	}