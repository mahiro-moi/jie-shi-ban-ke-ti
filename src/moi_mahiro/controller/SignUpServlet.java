package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import moi_mahiro.beans.Branches;
import moi_mahiro.beans.Departments;
import moi_mahiro.beans.User;
import moi_mahiro.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Branches branch = new Branches();
        List<Branches> branches = new UserService().getBranch(branch);
        request.setAttribute("branches", branches);
    	Departments department = new Departments();
        List<Departments> departments = new UserService().getDepartment(department);
        request.setAttribute("departments", departments);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

   @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        List<String> errorMessages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, errorMessages)) {

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
            user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
            user.setIsStopped(request.getParameter("is_stopped"));

            new UserService().register(user);

            response.sendRedirect("./UserManagement");
        } else {
            session.setAttribute("errorMessages", errorMessages);

            User user = new User();
            user.setName(request.getParameter("name"));
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            if(!request.getParameter("branch_id").equals("")) {
            user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
            }
            if(!request.getParameter("department_id").equals("")) {
            user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));
            }
            user.setIsStopped(request.getParameter("is_stopped"));
            request.setAttribute("user", user);

        	Branches branch = new Branches();
            List<Branches> branches = new UserService().getBranch(branch);
            request.setAttribute("branches", branches);

        	Departments department = new Departments();
            List<Departments> departments = new UserService().getDepartment(department);
            request.setAttribute("departments", departments);

            request.getRequestDispatcher("/signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String checkPass = request.getParameter("checkPassword");
        String name = request.getParameter("name");
        String branchId = request.getParameter("branch_id");
        String departmentId = request.getParameter("department_id");

        UserService userService = new UserService();
        User user = userService.idCheck(loginId);


        if(user != null) {
        	errorMessages.add("そのIDは既に使用されています");
        }
        if (StringUtils.isEmpty(loginId)) {
        	errorMessages.add("ログインIDを入力してください");
        }else if (!loginId.matches("^[a-zA-Z0-9]{6,20}")) {
        	errorMessages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
        }else if (loginId.contains(" ")) {
        	errorMessages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
        }
        if (StringUtils.isEmpty(password)) {
        	errorMessages.add("パスワードを入力してください");
        }else if(!password.matches("^[!-~]{6,20}")) {
        	errorMessages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
        }else if(loginId.contains(" ")) {
        	errorMessages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
        }
        if (!password.equals(checkPass)) {
    		errorMessages.add("確認用パスワードが一致しません");
        }
        if(StringUtils.isBlank(name)) {
        	errorMessages.add("ユーザー名を入力してください");
        }
        if (name.length() > 10 ) {
        	errorMessages.add("ユーザー名は10文字以下で入力してください");
        }
        if (branchId.equals("")) {
    		errorMessages.add("支店を選択してください");
        }
        if (departmentId.equals("")) {
    		errorMessages.add("部署・所属を選択してください");
        }
        if(!branchId.equals("1")) {
        	if(departmentId.equals("1") || departmentId.equals("2")) {
        		errorMessages.add("支店と部署・所属の組み合わせが不正です");
        	}
        }
        if(branchId.equals("1")) {
        	if(departmentId.equals("3") || departmentId.equals("4")) {
        		errorMessages.add("支店と部署・所属の組み合わせが不正です");
        	}
        }
        if (errorMessages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}