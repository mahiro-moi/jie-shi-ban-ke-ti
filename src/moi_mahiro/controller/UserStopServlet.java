package moi_mahiro.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import moi_mahiro.beans.Management;
import moi_mahiro.service.IsStoppedService;

	@WebServlet(urlPatterns = { "/isStopped" })
	public class UserStopServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	    	Management management = new Management();
	    	management.setId(Integer.parseInt(request.getParameter("id")));
	    	if(request.getParameter("isStopped").equals("0")) {
		    	management.setIsStopped("1");
	    	}else if(request.getParameter("isStopped").equals("1")){
		    	management.setIsStopped("0");
	    	}

	    	new IsStoppedService().isStopped(management);

	            response.sendRedirect("./UserManagement");
	    }
	}