package moi_mahiro.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import moi_mahiro.beans.Management;
import moi_mahiro.service.ManagementService;

@WebServlet(urlPatterns = { "/UserManagement" })
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


        @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<Management> managements = new ManagementService().getManagement();
        request.setAttribute("managements", managements);
        request.getRequestDispatcher("UserManagement.jsp").forward(request, response);

    }
}