package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import moi_mahiro.beans.Message;
import moi_mahiro.beans.User;
import moi_mahiro.service.MessageService;
import moi_mahiro.service.UserService;

@WebServlet(urlPatterns = { "/NewMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


        @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");
        User editUser = new UserService().getUser(loginUser.getLoginId());
        request.setAttribute("editUser", editUser);
        request.getRequestDispatcher("NewMessage.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> errorMessages = new ArrayList<String>();

        if (isValid(request, errorMessages)) {

            User user = (User) session.getAttribute("loginUser");
            Message message = new Message();
            message.setText(request.getParameter("text"));
            message.setSubject(request.getParameter("subject"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", errorMessages);

            User user = (User) session.getAttribute("loginUser");
            Message message = new Message();
            message.setText(request.getParameter("text"));
            message.setSubject(request.getParameter("subject"));
            message.setCategory(request.getParameter("category"));
            message.setUserId(user.getId());

            request.setAttribute("message", message);

            request.getRequestDispatcher("NewMessage.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String subject = request.getParameter("subject");
        String text = request.getParameter("text");
        String category = request.getParameter("category");

        if (StringUtils.isBlank(category)) {
            messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
        }
        if (StringUtils.isBlank(subject)) {
            messages.add("件名を入力してください");
        }
        if (30 < subject.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isBlank(text)) {
            messages.add("本文を入力してください");
        }
        if (1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}