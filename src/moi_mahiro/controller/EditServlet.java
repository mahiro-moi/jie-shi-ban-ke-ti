package moi_mahiro.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import moi_mahiro.beans.Branches;
import moi_mahiro.beans.Departments;
import moi_mahiro.beans.Management;
import moi_mahiro.beans.User;
import moi_mahiro.service.EditService;
import moi_mahiro.service.LoginService;
import moi_mahiro.service.ManagementService;
import moi_mahiro.service.UserService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

        User user = new User();

        if(StringUtils.isBlank(request.getParameter("id"))){
        	errorMessages.add("不正なパラメーターが入力されました");
        	session.setAttribute("errorMessages", errorMessages);

        	response.sendRedirect("./UserManagement");
        	return;
        }
        if(!request.getParameter("id").matches("^[0-9]{1,10}")){
        	errorMessages.add("不正なパラメーターが入力されました");
        	session.setAttribute("errorMessages", errorMessages);

        	response.sendRedirect("./UserManagement");
        	return;
        }
        if(!request.getParameter("id").matches("[+-]?\\d+")){
        	errorMessages.add("不正なパラメーターが入力されました");
        	session.setAttribute("errorMessages", errorMessages);

        	response.sendRedirect("./UserManagement");
        	return;
        }
        user.setId(Integer.parseInt(request.getParameter("id")));

        User users = new EditService().getUser(user);

        if(users.getId() == 0) {
        	errorMessages.add("不正なパラメーターが入力されました");
        	session.setAttribute("errorMessages", errorMessages);

        	response.sendRedirect("./UserManagement");
        	return;
        }
        request.setAttribute("users", users);

        Branches branch = new Branches();
        List<Branches> branches = new UserService().getBranch(branch);
        request.setAttribute("branches", branches);

        Departments department = new Departments();
        List<Departments> departments = new UserService().getDepartment(department);
        request.setAttribute("departments", departments);

        request.getRequestDispatcher("Edit.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

	    if (isValid(request, errorMessages)) {

	    	User user = new User();
	        user.setId(Integer.parseInt(request.getParameter("id")));
	        user.setName(request.getParameter("name"));
	        user.setLoginId(request.getParameter("loginId"));
	        user.setPassword(request.getParameter("password"));
	        user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
	        user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));

	        new EditService().register(user,request);

    		String login_id = request.getParameter("loginUserId");

    		LoginService loginService = new LoginService();
    		User userNew = loginService.loginEdit(login_id);

    		session.setAttribute("loginUser", userNew);

	        response.sendRedirect("./UserManagement");

    	}else {
    		session.setAttribute("errorMessages", errorMessages);
    		User user = new User();
        	user.setId(Integer.parseInt(request.getParameter("id")));
        	user.setName(request.getParameter("name"));
        	user.setLoginId(request.getParameter("loginId"));
        	user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
        	user.setDepartmentId(Integer.parseInt(request.getParameter("department_id")));


        	new EditService().getUser(user);
        	request.setAttribute("users", user);

        	Branches branch = new Branches();
            List<Branches> branches = new UserService().getBranch(branch);
            request.setAttribute("branches", branches);

            Departments department = new Departments();
            List<Departments> departments = new UserService().getDepartment(department);
            request.setAttribute("departments", departments);

        	request.getRequestDispatcher("Edit.jsp").forward(request, response);
    	}
    }
    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

    	String password = request.getParameter("password");
	    String checkPass = request.getParameter("checkPassword");
	    String loginId = request.getParameter("loginId");
	    String name = request.getParameter("name");
	    String branchId = request.getParameter("branch_id");
	    String departmentId = request.getParameter("department_id");

	    List<Management> managements = new ManagementService().getManagement();

	    UserService userService = new UserService();
        User user = userService.idCheck(loginId);

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLoginId(request.getParameter("login_id"));


        if (managements == null) {
    		errorMessages.add("該当ユーザーが存在しません");
        }
        if(!editUser.getLoginId().equals(loginId)) {
        	if(user != null) {
        		errorMessages.add("そのIDは既に使用されています");
        	}
        }
        if(StringUtils.isBlank(loginId)) {
        	errorMessages.add("ログインIDを入力してください");
        }else if (!loginId.matches("^[a-zA-Z0-9]{6,20}")) {
        	errorMessages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
        }
        if (loginId.contains(" ")) {
        	errorMessages.add("ログインIDは6文字以上20文字以下の半角英数字で入力してください");
        }
        if(password != "") {
        	if(!password.matches("^[!-~]{6,20}")) {
        		errorMessages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
        	}else if(loginId.contains(" ")) {
        		errorMessages.add("パスワードは6文字以上20文字以下の半角文字で入力してください");
        	}
        }
	    if (!password.equals(checkPass)) {
    		errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
        }
        if(StringUtils.isBlank(name)) {
        	errorMessages.add("ユーザー名を入力してください");
        }
        if (name.length() > 10 ) {
        	errorMessages.add("ユーザー名は10文字以下で入力してください");
        }
	    if(branchId.equals("1")) {
	    	if(departmentId.equals("3"))  {
	    		errorMessages.add("支店と部署・役職の組み合わせが不正です");
	    	}else if(departmentId.equals("4")) {
	    		errorMessages.add("支店と部署・役職の組み合わせが不正です");
	    	}
	    }
	    if(!branchId.equals("1"))
	    	if(departmentId.equals("1") || departmentId.equals("2")) {
	    		errorMessages.add("支店と部署・役職の組み合わせが不正です");
	    	}
    	if (errorMessages.size() == 0) {
            return true;
    	}

    	else {
            return false;
        }
    }
}