<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

		<script type="text/javascript">
			function checkStop(){
				if(window.confirm('ユーザーを停止しますか？')){
					return true;
				}
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
			function checkRevival(){
				if(window.confirm('ユーザーを復活しますか？')){
					return true;
				}
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>

	</head>
	<body>
	<div class =header>
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
		<span  style="float: right;" >
			<c:out value="${loginUser.name}　　"/>
			 <a href="logout">ログアウト</a></span>
	</div>
	<div class="title"><h2 align="center">ユーザー管理画面</h2></div>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
		<input type="hidden" name="id" value="${management.id}">
	</c:if>

	<div class="management">
	<table id=umTable>
		<tr>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>支店</th>
			<th>部署・役職</th>
			<th>復活/停止</th>
			<th>ステータス</th>
			<th>編集</th>
		</tr>
		<c:forEach items="${managements}" var="management">
				<tr>
				<form action="userManagement " method="post">
					<div class="management">
						<th><c:out value="${management.loginId}" /></th>
						<th><c:out value="${management.name}" /></th>
						<th><c:out value="${management.branchName}" /></th>
						<th><c:out value="${management.departmentName}" /></th>
					</div>
				</form>
				<c:if test = "${management.id  != loginUser.id}">
					<form action="isStopped " method="post" onSubmit="return checkStop()">
						<c:if test="${management.isStopped == 0  }">
							<input type="hidden" name="id" value="${management.id}">
							<input type="hidden" name="isStopped" value="${management.isStopped}">
							<th><input type="submit" value="停止" /></th>
						</c:if>
					</form>
					<form action="isStopped " method="post" onSubmit="return checkRevival()">
						<c:if test="${management.isStopped == 1  }">
							<input type="hidden" name="id" value="${management.id}">
							<input type="hidden" name="isStopped" value="${management.isStopped}">
							<th><input type="submit" value="復活"  /></th>
						</c:if>
					</form>
				</c:if>
				<c:if test = "${management.id  == loginUser.id}">
				<th>ログイン中</th>
				</c:if>
				<c:if test="${management.isStopped == 0  }">
						<th><a id="statas">使用中</a></th>
				</c:if>
				<c:if test="${management.isStopped == 1  }">
						<th><a id="statasStop">停止中</a></th>
				</c:if>
				<th><a href="edit?id=${management.id}" id="editButton">	編集</a></th>
				</tr>
		</c:forEach>
		</table>
	</div>
	<div class=return>
		<a href="./" id="returnButton" >戻る</a>
	</div>
	<div class="copyright"> Copyright(c)MahiroMoi</div>
	</body>
</html>