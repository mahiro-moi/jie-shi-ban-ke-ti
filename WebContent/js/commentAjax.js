
function checkMessage(){
				if(window.confirm('投稿を削除しますか？')){
				return true;
				}else{
					window.alert('投稿の削除がキャンセルされました');
					return false;
				}
			}
			function checkComment(){
				if(window.confirm('コメントを削除しますか？')){
				return true;
				}else{
					window.alert('コメントの削除がキャンセルされました');
					return false;
			}
}

$(function newomment() {
	$('button#commentBottun').on('click', function() {
		// 送信するデータを用意
		var form = $(this).parents("form#comment-form");
		var messageId = form.find('#messageId').val();
		var text = form.find('#text').val();
		var userId = form.find('#userId').val();
		var userName = form.find('#userName').val();
		var comment = { 'messageId': messageId, 'userId': userId, 'text': text, 'name': userName};
		var messageArea = $(this).parents("div.message");
		var messageNo = messageArea.find('#messageNo').val();


		//今日の日付データを変数nowDateに格納
		var nowDate = new Date();
		var y = nowDate.getFullYear();
		var m = nowDate.getMonth() + 1;
		var d = nowDate.getDate();
		var h = nowDate.getHours();
		var min = nowDate.getMinutes();

		var div = `
			<div class="commentAjax">
					<p1>@` + userName + `</p>
					<p1>` + text + `</p>
					<p1>` + y +'/'+ m + '/' + d +' ' + h +':' + min +`</p>
					<form action="deleteComment" method="post" onSubmit="return checkComment()">
						<div class="deleteButton">
							<input type="submit" value="コメント削除">
						</div>
					</form>
			</div>`;
			// Ajax通信処理
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'newComment',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if(data.is_success == 'true') {
				/* バリデーション通過 */
				// コメントデータを追加
				$("div.messages").find("#ajacComment_" + messageId).append($(div));
				// 全てのtextarea要素の中身を削除
				$(".comment-box").each(function(k, v) { $(this).val(""); })
			} else {
				/* バリデーションエラー */
				var errorArea = $("div.main-contents").find("div.errorMessages");
				// 入力フォームの上にエラーメッセージを表示させる
				data.errors.forEach(function(v, k) {
					// エラーメッセージの要素の数だけ表示させる
					errorArea.find("ul").append("<li>" + data.errors + "</li>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log(data);
			console.log('error!!');
		});
	});

});
