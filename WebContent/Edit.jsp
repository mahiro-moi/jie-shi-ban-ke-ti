<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー編集</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">

    <script type="text/javascript">
		function check(){
			if(window.confirm('この内容で編集しますか？')){
			return true;
			}else{
				window.alert('編集がキャンセルされました');
				return false;
			}
		}

	</script>
    </head>
    <body>
		<div class =header>
			<a href="./">ホーム</a>
			<span  style="float: right;" >
				<span id="loginUser"><c:out value="${loginUser.name}　　"/></span>
			<a href="logout">ログアウト</a>
			</span>

		</div>
		<div class="title"><h2 align="center">ユーザー編集画面</h2></div>
		<div class=edit-main-contents>
			<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
			<input type="hidden" name="id" value="${management.id}">
		</c:if>
		<div class="editUser">
			<form action="edit" method="post" onSubmit="return check()">
			<table id= "editTable">
					<input type="hidden" name="id" value="${users.id}">
					<input type="hidden" name="login_id" value="${users.loginId}">
					<input type="hidden" name ="loginUserId" value="${loginUser.id}">
					<input name="loginPassword" type="hidden" id="loginPassword" value ="${loginUser.password}"/>

				<tr>
					<th>ログインID</th>
					<th id="input"><input type ="text"name = loginId value = "${users.loginId}"/><a id="conditions">　(6文字以上20文字以下 半角英数字)</a></th>
				</tr>
				<tr>
					<th>パスワード</th>
					<th id="input"><input type ="password" name = password value = "${users.password}"/><a id="conditions">　(6文字以上20文字以下 半角文字)</a></th>
				</tr>

				<tr>
					<th>パスワード確認用</th>
					<th id="input"><input type ="password" name = checkPassword /></th>
				</tr>
				<tr>
					<th>ユーザー名</th>
					<th id="input"><input type ="text" name = name value = "${users.name}"/><a id="conditions">　(10文字以下)</a></th>
				</tr>
				<tr>
					<th>支店</th>
					<c:if test = "${users.loginId  != loginUser.loginId}">
					<th id="input">
						<select name="branch_id"  >
							<c:forEach items="${branches}" var="branch">
								<c:choose>
									<c:when test = "${branch.id  == users.branchId}">
										<option value="${branch.id}" selected>${branch.branchName}</option>
									</c:when>
									<c:otherwise>
										<option value="${branch.id}">${branch.branchName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</th>
					</c:if>
					<c:if test = "${users.loginId  == loginUser.loginId}">
						<th id="input">
							<select name="branch_id"  >
							<c:forEach items="${branches}" var="branch">
								<c:choose>
									<c:when test = "${branch.id  == users.branchId}">
										<option value="${branch.id}" selected>${branch.branchName}</option>
									</c:when>
								</c:choose>
							</c:forEach>
							</select>
						</th>
					</c:if>
				</tr>
				<tr>
					<th>部署・役職</th>
					<c:if test = "${users.loginId  != loginUser.loginId}">
					<th id="input"><select name="department_id">
						<c:forEach items="${departments}" var="department">
							<c:choose>
								<c:when test = "${department.id  == users.departmentId}">
									<option value="${department.id}" selected>${department.departmentName}</option>
								</c:when>
								<c:otherwise>
									<option value="${department.id}">${department.departmentName}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					</th>
					</c:if>
					<c:if test = "${users.loginId  == loginUser.loginId}">
						<th id="input"><select name="department_id">
							<c:forEach items="${departments}" var="department">
								<c:choose>
									<c:when test = "${department.id  == users.departmentId}">
										<option value="${department.id}" selected>${department.departmentName}</option>
									</c:when>
								</c:choose>
							</c:forEach>
							</select>
						</th>
					</c:if>
				</tr>
				</table>
				<div class="button">
					<input type="submit" value="登録" id="record" />
				</div>
			</form>
			<div class=return>
				<a href="./UserManagement" id="returnButton">戻る</a>
			</div>
    		</div>
    	</div>
    	<div class="copyright">Copyright(c)MahiroMoi</div>
    </body>
</html>