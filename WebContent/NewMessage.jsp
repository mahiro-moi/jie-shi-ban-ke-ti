<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	</head>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<body>
	<div class =header>
		<a href="./">ホーム</a>
		<span  style="float: right;" >
				<span id="loginUser"><c:out value="${loginUser.name}　　"/></span>
			<a href="logout">ログアウト</a>
			</span>
	</div>
	<div class="title"><h2 align="center">新規投稿画面</h2></div>
		<div class="newmessage-main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					 </ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

    		<form action="NewMessage " method="post">
    		<table id="newMessageTable">
				<tr>
					<th><label for="category">カテゴリー<br/>（10文字以下で入力してください）</label></tr>
				</tr>
				<tr>
					<th id="inputNewMessage"><input name="category" id="category" value ="${message.category}"/></th>
				</tr>
				<tr>
					<th><label for="subject">件名<br/>（30文字以下で入力してください）</label></th>
				</tr>
				<tr>
					<th id="inputNewMessage"><input name="subject" id="subject" value ="${message.subject}"/></th>
				</tr>
				<tr>
					<th>本文<br/>（1000文字以下で入力してください）</th>
				</tr>
				<tr>
					<th id="inputNewMessage"><textarea name="text" cols="100" rows="5" wrap="soft" class="text-box" >${message.text}</textarea><br/>
				</tr>
				</table>
				<div class="button">
					<input type="submit" value="投稿" id="record">
				</div>
				<div class=return>
					<a href="./" id="returnButton">戻る</a>
				</div>
			</form>
		</div>
		<div class="copyright"> Copyright(c)MahiroMoi</div>
	</body>
</html>