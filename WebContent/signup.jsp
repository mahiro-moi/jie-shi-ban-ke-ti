<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <body>
    <div class =header>
		<a href="./">ホーム</a>

		<span  style="float: right;" >
				<span id="loginUser"><c:out value="${loginUser.name}　　"/></span>
			<a href="logout">ログアウト</a>
			</span>

	</div>
		<div class="signup-main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<table id=signUpTable>
			<form action="signup" method="post">
				<tr>
					<th><label for="login_id">ログインID</label></th>
					<th id="input"><input name="login_id"id="login_id" value ="${user.loginId}" /><a id="conditions">　(6文字以上20文字以下 半角英数字)</a></th>
				</tr>
				<tr>
					<th><label for="password">パスワード</label>
					<th id="input"><input name="password" type="password" id="password" /><a id="conditions">　(6文字以上20文字以下 半角文字)</a></th>
				</tr>
				<tr>
					<th ><label for="checkPassword">確認用パスワード</label></th>
					<th id="input"><input name="checkPassword" type="password" id="checkPassword" /></th>
				<tr>
					<th><label for="name">ユーザー名</label></th>
					<th id="input"><input name="name" id="name" value ="${user.name}" /><a id="conditions">　(10文字以下)</a></th>
				</tr>
				<tr>
					<th><label for="branch_id">支店</label></th>
					<th id="input"><select name="branch_id"  >
						<option value="">選択してください</option>
						<c:forEach items="${branches}" var="branch">
							<c:choose>
								<c:when test = "${branch.id  == user.branchId}">
									<option value="${branch.id}" selected>${branch.branchName}</option>
								</c:when>
								<c:otherwise>
									<option value="${branch.id}">${branch.branchName}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					</th>
				<tr>
					<th><label for="department_id">部署・役職</label></th>
						<th id="input"><select name="department_id"  >
							<option value="">選択してください</option>
							<c:forEach items="${departments}" var="department">
								<c:choose>
									<c:when test = "${department.id  == user.departmentId}">
										<option value="${department.id}" selected>${department.departmentName}</option>
									</c:when>
									<c:otherwise>
										<option value="${department.id}">${department.departmentName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
						</th>
				</tr>
				</table>
				<div class="button">
					<input type="submit" value="登録" id="record"/>
				</div>
				<div class=return>
					<a href="./UserManagement" id="returnButton">戻る</a>
				</div>
			</form>
			<div class="copyright">Copyright(c)MahiroMoi</div>
		</div>
	</body>
</html>