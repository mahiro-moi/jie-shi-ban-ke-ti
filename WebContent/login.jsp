<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ログイン</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<h1>掲示板課題</h1>
	<h2 align="center">ログイン画面</h2>

	<div class="login-main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</c:if>

		<form action="login" method="post">
			<table id="loginBox">
				<tr>
					<th><label for="login_id" >ログインID</label></th>
					<th><input name="login_id" id="login_id" value="${loginUser.loginId}"/> </th>
				</tr>
				<tr>
					<th><label for="password">パスワード</label></th>
					<th><input name="password" type="password" id="password"/> </th>
			</tr>
			</table>
			<div class = "button">
				<input type="submit" value="ログイン" id="loginButton"/>
			</div>
		</form>
		<div class="copyright"> Copyright(c)MahiroMoi</div>
	</div>
	</body>
</html>