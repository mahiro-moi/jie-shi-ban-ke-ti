<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String nowDate = (new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:SS").format(new java.util.Date())); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板課題</title>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="./js/commentAjax.js"></script>

        <script type="text/javascript">
			function checkMessage(){
				if(window.confirm('投稿を削除しますか？')){
				return true;
				}else{
					window.alert('投稿の削除がキャンセルされました');
					return false;
				}
			}
			function checkComment(){
				if(window.confirm('コメントを削除しますか？')){
				return true;
				}else{
					window.alert('コメントの削除がキャンセルされました');
					return false;
				}
			}
		</script>
		<link href="./css/style.css" rel="stylesheet" type="text/css">

    </head>
    <body>
		<div class =header>
			<a href="./">ホーム</a>
			<a href="NewMessage">新規投稿</a>
			<c:if test = "${loginUser.branchId == 1 }">
				<a href="UserManagement">ユーザー管理</a>
			</c:if>
				<span  style="float: right;" ><a href="logout">ログアウト</a></span>
		</div>
		<div class="main-contents">
			<div class="profile">
				<div class="loginName"><h4>ユーザー名: <c:out value="${loginUser.name}" /></h4></div>
			</div>
			<form action="./" method="get">
				<div class="sort" >
					<label for="category" >カテゴリー：</label>
						<input type ="text" name = category  value="${category}" id="categoryLabel"/><br />
					<label for="start_at" >期間指定　：</label>
						<input type="date" min="2019-01-01" max="2099-12-31" name="start_at" value="${start_at}"  />
					<label for="end_at">～</label>
						<input type="date" min="2019-01-01" max="2099-12-31" name="end_at" value="${end_at}"/>
					<div class="button">
						<input type="submit" value="検索">
					</div>
				</div>
			</form>
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" /></li>
					</c:forEach>

				</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
			<div class="messageArea">
				<c:forEach items="${messages}" var="message">
					<c:if test= "${message.id != 0}">
						<div class="messages">
							<div class="message" >
								<div class="category">カテゴリー：<c:out value="${message.category}" /></div>
								<div class="subject">件名:<c:out value="${message.subject}" /></div><br/>
								<div class="messageText">本文：<br/><c:out value="${message.text}" /></div><br/>
								<div class="createdDate">登録日時：<c:out value="${message.createdDate}" /></div>
								<div class="messageId">Id：<c:out value="${message.id}" /></div>
								<div class="name">登録者：<c:out value="${message.name}" /></div>
								<input type="hidden" id="messageNo" name="messageNo" value="${message.id}">
								<c:if test = "${loginUser.id == message.userId }">
								<form action="deleteMessage" method="post" onSubmit="return checkMessage()">
									<input type="hidden" name="messageId" value="${message.id}">
									<div class="deleteButton"><input type="submit" value="投稿削除">
									</div>
								</form>
							</c:if>
							</div>
							<div  class="comments">
								<c:forEach items="${comments}" var="comment">
									<c:if test="${message.id == comment.messageId}">
										<div class="comment" id="${message.id}">
											<div class="name">@<c:out value="${comment.name}" /></div>
											<div class="text"><c:out value="${comment.text}" /></div>
											<div class="createdDate"><c:out value="${comment.createdDate}" /></div>
											<c:if test = "${loginUser.id == comment.userId }">
												<form action="deleteComment" method="post" onSubmit="return checkComment()">
													<input type="hidden" name="commentId" value="${comment.id}">
													<div class="deleteButton">
													<input type="submit" value="コメント削除">
													</div>
												</form>
											</c:if>
										</div>
									</c:if>
								</c:forEach>
							</div>
							<div id="ajacComment_${message.id}"></div>

							<div class="form-area">
								<form id="comment-form">
									<textarea name="comment" cols="110" rows="2" wrap="soft" class="comment-box" id="text"></textarea><br />
									<input type="hidden" name="messageId" id="messageId" value="${message.id}">
									<input type="hidden" name="uerId"  id="userId" value="${loginUser.id}">
									<input type="hidden" name="userName"  id="userName" value="${loginUser.name}">
									 <button type="button" id="commentBottun">コメントする</button>
     				 			</form>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
			<div class="copyright"> Copyright(c)MahiroMoi></div>
		</div>
	</body>

</html>